import { IResolvers, makeExecutableSchema } from 'graphql-tools';
import express from 'express';
import compression from 'compression';
import cors from 'cors';
import { GraphQLSchema } from 'graphql';
import graphQlHttp from 'express-graphql';
const app = express();

app.use('*', cors());
app.use(compression());

const typeDefs = `
    type Query {
        hola: String!
        holaConNombre(nombre: String!): String!
        holaGraphQL: String!
    }
`;

const resolvers: IResolvers = {
  Query: {
    hola(): string {
      return "Hola Mundo";
    },
    holaConNombre(__: void, { nombre }): string {
      return `Hola Mundo ${nombre}`;
    },
    holaGraphQL(): string {
      return "Hola Mundo al curso GraphQL";
    },
  },
};
const schema: GraphQLSchema = makeExecutableSchema({
  typeDefs,
  resolvers
})
app.use('/', graphQlHttp({
  schema,
  graphiql: true 
}));

app.listen(
  { port: 5300 },
  () => console.log('http://localhost:5300/graphql')
);


